package com.molange.app.gateway.filter;

import com.molange.app.gateway.util.AppJwtUtil;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import reactor.netty.http.server.HttpServerRequest;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/13 13:49
 *进行登录的校验，并进行服务的分发
 */
@Slf4j
@Component
public class AppFilter implements Ordered, GlobalFilter {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info("检测到请求");
        //1.获取request和response对象
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        //2.判断是否是看视频，或者是登录与注册，如果是的话，进行放行，如果不是验证token
        String path = request.getURI().getPath();
        if (path.contains("/feed")||path.contains("/login")||path.contains("/register")||path.contains("/douyin/publish/action/")){
           log.info("本次请求已经放行");
            //如果是以上请求，放行
            return chain.filter(exchange);
        }
        log.info("开始进行验签");
        //如果不是，需要堆用户的登录状态进行校验，获取视频上传的token
        String publishToken = request.getHeaders().getFirst("token");
        log.info("这是投稿中的token"+publishToken);
        //判断是否存在token,没有token
        String token1 = request.getQueryParams().getFirst("token");
        System.out.println(token1);
        if (StringUtils.isBlank(token1)){
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }
        //有token,判断token是否有效
        Claims claimsBody = AppJwtUtil.getClaimsBody(token1);
        try {
            //是否是过期
            int result = AppJwtUtil.verifyToken(claimsBody);
            if(result == 1 || result  == 2){
                response.setStatusCode(HttpStatus.UNAUTHORIZED);
                return response.setComplete();
            }
        }catch (Exception e){
            e.printStackTrace();
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }
        //在这里获取用户登录的token,并获取token的数据
        Integer userId = (Integer) claimsBody.get("id");
        System.out.println("这里是在网关中获取的token中的用户名"+userId);
        //将id放入到请求头中，放行
        ServerHttpRequest serverHttpRequest = request.mutate().header("userId",String.valueOf(userId))
                .build();
        //重置header
        exchange.mutate().request(serverHttpRequest).build();
        //6.放行
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
