package com.molange.app.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/13 13:04
 */
@SpringBootApplication
public class AppGateWayApplication {
    public static void main(String[] args) {
        SpringApplication.run(AppGateWayApplication.class, args);
    }
}
