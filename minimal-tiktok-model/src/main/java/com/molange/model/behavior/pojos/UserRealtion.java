package com.molange.model.behavior.pojos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/2/26 9:54
 */
@TableName("user_relation")
@Data
public class UserRealtion {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("user_id")
    private Integer userId;

    @TableField("relation_id")
    private Integer relationId;

    @TableField("create_time")
    private Date createTime;

    @TableField("relation_two")
    private Integer relationTwo;
}
