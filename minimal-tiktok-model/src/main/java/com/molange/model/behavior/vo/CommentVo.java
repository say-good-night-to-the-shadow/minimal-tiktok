package com.molange.model.behavior.vo;

import com.molange.model.user.vo.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/2/24 8:33
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentVo {

    /**
     * 评论id
     */
    private Integer id;

    /**
     * 用户信息
     */
    private User user;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 时间
     */
    private String create_date;



}
