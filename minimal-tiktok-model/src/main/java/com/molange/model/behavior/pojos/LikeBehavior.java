package com.molange.model.behavior.pojos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/31 14:50
 */
@Data
@TableName("like_behavior")
public class LikeBehavior {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("user_id")
    private Integer userId;

    @TableField("publish_id")
    private Integer publishId;




}
