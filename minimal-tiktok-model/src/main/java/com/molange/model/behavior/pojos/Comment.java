package com.molange.model.behavior.pojos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/2/21 22:51
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("comment")
public class Comment {
    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户id
     */
    @TableField("user_id")
    private Integer userId;

    /**
     * 视频id
     */
    @TableField("publish_id")
    private Integer publishId;

    /**
     * 评论内容
     */
    @TableField("comment_text")
    private String CommentText;

    /**
     *0正常
     *1锁定
     */
    @TableField("status")
    private Integer status;


    @TableField("create_time")
    private Date creatTime;

}
