package com.molange.model.feed;

import com.molange.model.user.vo.Author;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/29 20:22
 */
 
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Video implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 视频的唯一标识
     */
    private Integer id;

    /**
     * 用户信息
     */
    private Author author;

    /**
     * 视频播放地址
     */
    private String play_url;
    /**
     * 视频封面
     */
    private String cover_url;

    /**
     * 视频点赞数
     */
    private Integer favorite_count;

    /**
     * 视频评论数
     */
    private Integer comment_count;

    /**
     * 是否点赞
     */
    private Boolean is_favorite;

    /**
     * 视频标题
     */
    private String title;



    public Boolean getIs_favorite() {
        return is_favorite;
    }

    public void setIs_favorite(Boolean is_favorite) {
        this.is_favorite = is_favorite;
    }
}
