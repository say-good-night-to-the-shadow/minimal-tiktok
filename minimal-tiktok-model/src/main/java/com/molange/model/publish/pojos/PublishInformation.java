package com.molange.model.publish.pojos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/30 22:02
 */
@Data
@TableName("publish_information")
public class PublishInformation implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("publish_id")
    private Integer publishId;

    @TableField("likes")
    private Integer likes;

    @TableField("comment")
    private Integer comment;
}
