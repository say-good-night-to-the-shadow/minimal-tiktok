package com.molange.model.publish.pojos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/28 10:29
 */
@Data
@TableName("publish_data")
public class Publish implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     *0正常
     *1锁定
     */
    @TableField("status")
    private Integer status;

    @TableField("user_id")
    private Integer userId;

    @TableField("url")
    private String url;

    @TableField("creat_time")
    private Date creatTime;

    @TableField("title")
    private String title;

    @TableField("picture_url")
    private String pictureUrl;


}
