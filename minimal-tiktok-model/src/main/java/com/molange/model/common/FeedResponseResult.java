package com.molange.model.common;

import lombok.Data;

import java.io.Serializable;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/30 17:51
 */
@Data
public class FeedResponseResult <T> implements Serializable {

    private static final long serialVersionUID = 1L;
    //返回状态码
    private Integer status_code;

    //返回状态描述
    private String status_msg;

    private T video_list;

    private Integer next_time;

    public FeedResponseResult(Integer status_code, String status_msg, T video_list, Integer next_time) {
        this.status_code = status_code;
        this.status_msg = status_msg;
        this.video_list = video_list;
        this.next_time = next_time;
    }
}
