package com.molange.model.common;

import java.io.Serializable;
/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/13 11:31
 *----------------------
 * 通用的结果返回类
 * @param <T>
 */

public class ResponseResult<T> implements Serializable {

    //返回状态码
    private Integer status_code;

    //返回状态描述
    private String status_msg;

    //用户信息
    private Integer user_id;

    //返回token
    private String token;

    //返回对象类型
    private T data;

    private Integer next_time;


    public ResponseResult(Integer status_code, String status_msg, T data, Integer next_time) {
        this.status_code = status_code;
        this.status_msg = status_msg;
        this.data = data;
        this.next_time = next_time;
    }

    public ResponseResult() {
        this.status_code=200;
    }

    public ResponseResult(Integer status_code, T data) {
        this.status_code = status_code;
        this.data = data;
    }

    public ResponseResult(Integer status_code, String status_msg, T data) {
        this.status_code = status_code;
        this.status_msg = status_msg;
        this.data = data;
    }

    public ResponseResult(Integer status_code, String status_msg) {
        this.status_code = status_code;
        this.status_msg = status_msg;
    }

    public static ResponseResult errorResult(int status_code, String status_msg) {
        ResponseResult result = new ResponseResult();
        return result.error(status_code, status_msg);
    }

    public static ResponseResult okResult(int status_code, String status_msg) {
        ResponseResult result = new ResponseResult();
        return result.ok(status_code, null, status_msg);
    }
    public static ResponseResult okResult(int status_code,Integer user_id, String token) {
        ResponseResult result = new ResponseResult();
        return result.ok(status_code, user_id, token);
    }

    public static ResponseResult okResult(int status_code,Integer user_id, String token,String status_msg) {
        ResponseResult result = new ResponseResult();
        return result.ok(status_code, user_id, token,status_msg);
    }

    public ResponseResult<?> error(Integer code, String msg) {
        this.status_code = code;
        this.status_msg = msg;
        return this;
    }

    public ResponseResult<?> ok(Integer code, T data) {
        this.status_code = code;
        this.data = data;
        return this;
    }

    public ResponseResult<?> ok(Integer code, Integer user_id,String token) {
        this.status_code = code;
        this.user_id=user_id;
        this.token=token;
        return this;
    }

    public ResponseResult<?> ok(Integer code, Integer user_id,String token,String status_msg) {
        this.status_code = code;
        this.user_id=user_id;
        this.token=token;
        this.status_msg=status_msg;
        return this;
    }

    public ResponseResult<?> ok(Integer code, T data, String msg) {
        this.status_code = code;
        this.data = data;
        this.status_msg = msg;
        return this;
    }

    public ResponseResult<?> ok(T data) {
        this.data = data;
        return this;
    }

    public Integer getStatus_code() {
        return status_code;
    }

    public void setStatus_code(Integer status_code) {
        this.status_code = status_code;
    }

    public String getStatus_msg() {
        return status_msg;
    }

    public void setStatus_msg(String status_msg) {
        this.status_msg = status_msg;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
