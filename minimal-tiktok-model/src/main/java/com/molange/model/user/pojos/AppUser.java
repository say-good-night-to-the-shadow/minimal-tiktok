package com.molange.model.user.pojos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/13 21:02
 */
@Data
@TableName("app_user")
public class AppUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 密码、通信等加密盐
     */
    @TableField("salt")
    private String salt;

    /**
     * 用户名
     */
    @TableField("username")
    private String username;

    /**
     * 密码,md5加密
     */
    @TableField("password")
    private String password;

    /**
     *0正常
     *1锁定
     */
    @TableField("status")
    private Integer status;

    /**
     * 注册时间
     */
    @TableField("created_time")
    private Date createdTime;
}
