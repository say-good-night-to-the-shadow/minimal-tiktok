package com.molange.model.user.vo;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/17 14:23
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * User
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {
    /**
     * 关注总数
     */
    private long follow_count;
    /**
     * 粉丝总数
     */
    private long follower_count;
    /**
     * 用户id
     */
    private long id;
    /**
     * true-已关注，false-未关注
     */
    private boolean is_follow;
    /**
     * 用户名称
     */
    private String name;

    public boolean setIs_follow() {
        return is_follow;
    }

    public void setIs_follow(boolean is_follow) {
        this.is_follow = is_follow;
    }
}
