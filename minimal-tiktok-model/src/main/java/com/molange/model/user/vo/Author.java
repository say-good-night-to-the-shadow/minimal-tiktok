package com.molange.model.user.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/29 20:38
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Author  implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 关注总数
     */
    private long follow_count;
    /**
     * 粉丝总数
     */
    private long follower_count;
    /**
     * 用户id
     */
    private long id;
    /**
     * true-已关注，false-未关注
     */
    private boolean is_follow;
    /**
     * 用户名称
     */
    private String name;

    public boolean getIs_follow() {
        return is_follow;
    }

    public void setIs_follow(boolean is_follow) {
        this.is_follow = is_follow;
    }
}
