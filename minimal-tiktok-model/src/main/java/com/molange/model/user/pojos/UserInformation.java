package com.molange.model.user.pojos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/15 20:12
 */
@Data
@TableName("user_information")
public class UserInformation implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户id
     */
    @TableField("user_id")
    private Integer userId;

    /**
     * 用户名名称
     */
    @TableField("name")
    private String name;

    /**
     * 用户的关注数
     */
    @TableField("follow_count")
    private Integer followCount;

    /**
     * 用户的粉丝数
     */
    @TableField("follower_count")
    private Integer followerCount;

}
