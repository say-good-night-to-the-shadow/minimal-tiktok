package com.molange.publish.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.molange.model.publish.pojos.PublishInformation;
import com.molange.model.user.pojos.AppUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/30 22:06
 */
@Mapper
public interface PublishInformationMapper extends BaseMapper<PublishInformation> {
    //List<PublishInformation> selectBatchById(@Param("publishInformationList") List publishInformationList);
}
