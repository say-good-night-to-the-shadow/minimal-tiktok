package com.molange.publish.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.molange.model.publish.pojos.Publish;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/28 10:50
 */
@Mapper
public interface PublishMapper extends BaseMapper<Publish> {

    List<Publish> getPublishListLimit(@Param("data")Date date);

    List<Publish> loadMorePublish(Date date);

    //List<Publish> getPublishListByPublsihId(@Param("publishList") List publishList);
}
