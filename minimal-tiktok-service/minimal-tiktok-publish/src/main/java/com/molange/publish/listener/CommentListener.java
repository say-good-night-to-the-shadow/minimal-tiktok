package com.molange.publish.listener;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.molange.model.publish.pojos.PublishInformation;
import com.molange.publish.service.PublishInformationService;
import org.checkerframework.checker.units.qual.A;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/2/26 14:49
 */
@Component
public class CommentListener {

    @Autowired
    private PublishInformationService publishInformationService;

    /**
     * 评论监控微服务模块
     * @param publishId
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "comment.queue"),
            exchange = @Exchange(name = "tiktok.topic", type = ExchangeTypes.TOPIC),
            key = "behavior.comment"
    ))
    public void addCommentLisenter(Integer publishId){
        LambdaQueryWrapper<PublishInformation> queryWrapper=new LambdaQueryWrapper();
        queryWrapper.eq(PublishInformation::getPublishId,publishId);
        PublishInformation one = publishInformationService.getOne(queryWrapper);
        one.setComment(one.getComment()+1);
        publishInformationService.save(one);
    }

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "comment.queue"),
            exchange = @Exchange(name = "tiktok.topic", type = ExchangeTypes.TOPIC),
            key = "behavior.removeComment"
    ))
    public void removeCommentLisenter(Integer publishId){
        LambdaQueryWrapper<PublishInformation> queryWrapper=new LambdaQueryWrapper();
        queryWrapper.eq(PublishInformation::getPublishId,publishId);
        PublishInformation one = publishInformationService.getOne(queryWrapper);
        if (one.getComment()==0){
            return;
        }
        one.setComment(one.getComment()-1);
        publishInformationService.save(one);
    }
}
