package com.molange.publish.listener;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.molange.model.publish.pojos.PublishInformation;
import com.molange.publish.service.PublishInformationService;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 负责用户行为端的监听
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/31 22:01
 */
@Component
public class LikeBehaviorListener {
    @Autowired
    private PublishInformationService publishInformationService;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "like.queue"),
            exchange = @Exchange(name = "tiktok.topic", type = ExchangeTypes.TOPIC),
            key = "behavior.like"
    ))
    public void LikeBehaviorLisenter(Integer publishId){
        LambdaQueryWrapper<PublishInformation> queryWrapper=new LambdaQueryWrapper();
        queryWrapper.eq(PublishInformation::getPublishId,publishId);
        PublishInformation one = publishInformationService.getOne(queryWrapper);
        one.setLikes(one.getLikes()+1);
        publishInformationService.updateById(one);
    }


    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "unlike.queue"),
            exchange = @Exchange(name = "tiktok.topic", type = ExchangeTypes.TOPIC),
            key = "behavior.unlike"
    ))
    public void UnLikeBehaviorLisenter(Integer publishId){
        LambdaQueryWrapper<PublishInformation> queryWrapper=new LambdaQueryWrapper();
        queryWrapper.eq(PublishInformation::getPublishId,publishId);
        PublishInformation one = publishInformationService.getOne(queryWrapper);
        one.setLikes(one.getLikes()-1);
        publishInformationService.updateById(one);
    }
}
