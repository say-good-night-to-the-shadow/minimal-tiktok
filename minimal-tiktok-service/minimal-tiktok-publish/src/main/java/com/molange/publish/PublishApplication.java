package com.molange.publish;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/18 12:02
 */ 
@SpringBootApplication
public class PublishApplication {
    public static void main(String[] args) {
        SpringApplication.run(PublishApplication.class,args);
    }

}
