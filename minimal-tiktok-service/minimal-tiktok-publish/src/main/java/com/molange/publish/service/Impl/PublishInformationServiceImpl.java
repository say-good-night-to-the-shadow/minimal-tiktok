package com.molange.publish.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.molange.model.publish.pojos.PublishInformation;
import com.molange.publish.mapper.PublishInformationMapper;
import com.molange.publish.service.PublishInformationService;
import org.springframework.stereotype.Service;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/30 22:08
 */
@Service
public class PublishInformationServiceImpl extends ServiceImpl<PublishInformationMapper, PublishInformation>
        implements PublishInformationService {

}
