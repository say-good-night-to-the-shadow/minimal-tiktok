package com.molange.publish.controller;

import com.molange.model.common.ResponseResult;
import com.molange.model.publish.pojos.Publish;
import com.molange.model.publish.pojos.PublishInformation;
import com.molange.publish.service.PublishService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/19 12:14
 */
@RestController
@RequestMapping("/douyin/publish")
@Slf4j
public class PublishController {

    @Autowired
    private PublishService publishService;

    /**
     * 用户投稿接口
     * @param token
     * @param file
     * @param title
     * @return
     */
    @PostMapping("/action")
    private ResponseResult PostVideoSubmission(@RequestParam(value = "token") String token,
                                               @RequestParam(value ="data")MultipartFile file,
                                               @RequestParam(value = "title") String title){
        log.info("用户投稿接口接受到请求");
        if (StringUtils.isBlank(token)){
            log.info("请求失败！");
            return ResponseResult.errorResult(100,"登录状态异常");
        }
        if (StringUtils.isBlank(title)){
            log.info("请求失败！");
            return ResponseResult.errorResult(100,"标题不能为空");
        }
        return publishService.saveFile(token,file,title);
    }

    /**
     * 获取视频流
     * @param date
     * @param num
     * @return
     */
    @PostMapping("/load")
    private List<Publish> getPublish(String date,int num){
        log.info("接口被feign调用，开始获取视频集合列表");
        return publishService.loadPublish(date,num);
    }

    /**
     * 根据publish的id返回publish信息数据
     * @param
     * @return
     */
    @GetMapping("/gtePublishDataInformation")
    private  Publish gtePublishData(@RequestParam("publishId") Integer publishId){
        return publishService.loadPublish(publishId);
    }

    /**
     * 根据publish的id返回具体数据
     * @param
     * @return
     */
    @GetMapping("/getPublishInformation")
    private  PublishInformation getPublishInformation(@RequestParam("publishId") Integer publishId){
        return publishService.loadPublishInformation(publishId);
    }
}
