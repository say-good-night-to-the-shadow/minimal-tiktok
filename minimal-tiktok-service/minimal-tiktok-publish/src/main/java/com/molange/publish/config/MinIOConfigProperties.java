package com.molange.publish.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;


import java.io.Serializable;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/28 12:44
 */
@Data
@ConfigurationProperties(prefix = "minio")  // 文件上传 配置前缀file.oss
public class MinIOConfigProperties implements Serializable {

    private String accessKey;
    private String secretKey;
    private String bucket;
    private String endpoint;
    private String readPath;
}
