package com.molange.publish.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.molange.model.common.ResponseResult;
import com.molange.model.publish.pojos.Publish;
import com.molange.model.publish.pojos.PublishInformation;
import com.molange.model.user.pojos.AppUser;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/28 9:54
 */
public interface PublishService extends IService<Publish> {
    ResponseResult saveFile(String token, MultipartFile file, String title);

    List<Publish> loadPublish(String date, int num);

    Publish loadPublish(Integer publishList);

    PublishInformation loadPublishInformation(Integer publishInformationList);

}
