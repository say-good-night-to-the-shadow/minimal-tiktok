package com.molange.publish.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.molange.model.publish.pojos.PublishInformation;
import com.molange.model.user.pojos.AppUser;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/30 22:07
 */
public interface PublishInformationService extends IService<PublishInformation> {
}
