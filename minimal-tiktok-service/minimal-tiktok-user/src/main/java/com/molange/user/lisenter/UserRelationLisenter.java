package com.molange.user.lisenter;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.molange.model.user.pojos.UserInformation;
import com.molange.user.service.UserInformationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/2/27 12:30
 */
@Component
@Slf4j
public class UserRelationLisenter {

    @Autowired
    private UserInformationService userInformationService;

    /**
     * 用于添加用户关注数据
     * @param
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "like.queue"),
            exchange = @Exchange(name = "tiktok.topic", type = ExchangeTypes.TOPIC),
            key = "behavior.addUserFollowCount"
    ))
    public void UserFollowCountLisenter(Integer userId) {
        log.info("开始添加用户关注");
        LambdaQueryWrapper<UserInformation> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.eq(UserInformation::getUserId, userId);
        UserInformation one = userInformationService.getOne(queryWrapper);
        //关注人数加以
        one.setFollowCount(one.getFollowCount()+1);
        log.info("添加完成");
        userInformationService.saveOrUpdate(one);
    }


    /**
     * 粉丝数监控
     * @param userId
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "like.queue"),
            exchange = @Exchange(name = "tiktok.topic", type = ExchangeTypes.TOPIC),
            key ="behavior.addUserFollowerCount"
    ))
    public void UserFollowerCountLisenter(Integer userId) {
        LambdaQueryWrapper<UserInformation> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.eq(UserInformation::getUserId, userId);
        UserInformation one = userInformationService.getOne(queryWrapper);
        //关注人数加以
        one.setFollowerCount(one.getFollowerCount()+1);
        userInformationService.saveOrUpdate(one);
    }

    /**
     * 删除用户关注数
     * @param userId
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "like.queue"),
            exchange = @Exchange(name = "tiktok.topic", type = ExchangeTypes.TOPIC),
            key = "behavior.deleteUserFollowCount"
    ))
    public void deleteUserFollowCountLisenter(Integer userId) {
        LambdaQueryWrapper<UserInformation> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.eq(UserInformation::getUserId, userId);
        UserInformation one = userInformationService.getOne(queryWrapper);
        //关注人数加以
        if (one.getFollowCount()==0){
            return;
        }
        one.setFollowCount(one.getFollowCount()-1);
        userInformationService.saveOrUpdate(one);
    }

    /**
     * 删除关注用户的粉丝数
     * @param userId
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "like.queue"),
            exchange = @Exchange(name = "tiktok.topic", type = ExchangeTypes.TOPIC),
            key = "behavior.deleteUserFollowerCount"
    ))
    public void deleteUserFollowerCountLisenter(Integer userId) {
        LambdaQueryWrapper<UserInformation> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.eq(UserInformation::getUserId, userId);
        UserInformation one = userInformationService.getOne(queryWrapper);
        //关注人数加以
        if (one.getFollowerCount()==0){
            return;
        }
        one.setFollowCount(one.getFollowerCount()-1);
        userInformationService.saveOrUpdate(one);
    }
}
