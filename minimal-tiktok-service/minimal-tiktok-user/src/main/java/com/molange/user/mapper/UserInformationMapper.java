package com.molange.user.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.molange.model.user.pojos.UserInformation;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/16 9:58
 */
@Mapper
public interface UserInformationMapper  extends BaseMapper<UserInformation> {
}
