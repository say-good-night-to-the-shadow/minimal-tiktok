package com.molange.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.molange.model.common.ResponseResult;
import com.molange.model.user.pojos.UserInformation;
import com.molange.model.user.vo.User;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/16 9:58
 */
public interface UserInformationService extends IService<UserInformation> {
    /**
     * 用户信息获取接口
     * @param user_id
     * @return
     */
    ResponseResult getUserInformation(String user_id,String token);

    User getUserInformationByFeign(String user_id, String token);
}
