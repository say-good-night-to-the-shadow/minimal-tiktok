package com.molange.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.molange.model.common.ResponseResult;
import com.molange.model.user.pojos.AppUser;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/13 21:17
 */

public interface AppUserService  extends IService<AppUser> {

    ResponseResult register(String username, String password);

    ResponseResult login(String username, String password);
}
