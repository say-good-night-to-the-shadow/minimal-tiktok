package com.molange.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.molange.model.common.ResponseResult;
import com.molange.model.user.pojos.AppUser;
import com.molange.model.user.pojos.UserInformation;
import com.molange.model.user.pojos.UserRelationship;
import com.molange.model.user.vo.User;
import com.molange.user.mapper.UserInformationMapper;
import com.molange.user.service.AppUserService;
import com.molange.user.service.UserInformationService;
import com.molange.user.service.UserRelationshipService;
import com.molange.user.utils.AppJwtUtil;
import com.molange.user.utils.AppThreadLocalUtil;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/16 9:59
 * 用户个人信息服务实现
 */
@Service
@Slf4j
public class UserInformationServiceImpl extends ServiceImpl<UserInformationMapper, UserInformation>
        implements UserInformationService {

    @Autowired
    private AppUserService appUserService;

    @Autowired
    private UserRelationshipService relationshipService;

    /**
     * 获取用户的所有信息
     * @param user_id 用户id
     * @param token toekn
     * @return  返回用户的个人信息
     */
    @Override
    public ResponseResult getUserInformation(String user_id,String token) {
        log.info("开始获取用户信息");
        //查询数据库看是否存在数据
        LambdaQueryWrapper<UserInformation> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(UserInformation::getUserId,user_id);
        UserInformation userInformationObject = getOne(queryWrapper);
        if (null==userInformationObject){
            return ResponseResult.errorResult(100,"当前用户不存在");
        }
        //查到了数据库中的数据,如果token为空，或者token跟传入的id一致，直接返回用户数据
        Integer id1 = AppThreadLocalUtil.getUser().getId();
        User user = copyBean(new User(), userInformationObject);
        if (StringUtils.isBlank(token)||user_id.equals(String.valueOf(id1))){
            user.setIs_follow(false);
            log.info("没有获取到token");
            return new ResponseResult(0,"请求成功",user);
        }
        //如果有token,说明用户登录了，需要判断关注状态
        AppUser userId = AppThreadLocalUtil.getUser();
        Integer id = userId.getId();
        LambdaQueryWrapper<UserRelationship> relationshipLambdaQueryWrapper=new LambdaQueryWrapper<>();
        relationshipLambdaQueryWrapper.eq(UserRelationship::getUserId,id);
        relationshipLambdaQueryWrapper.eq(UserRelationship::getFollowId,user_id);
        UserRelationship one = relationshipService.getOne(relationshipLambdaQueryWrapper);
        if (null==one){
            //没有关注
            user.setIs_follow(false);
            return new ResponseResult(0,"请求成功",user);
        }
        user.setIs_follow(true);
        log.info("用户信息装填完成");
        return new ResponseResult(0,"请求成功",user);
    }

    @Override
    public User getUserInformationByFeign(String user_id, String token) {
        log.info("开始获取用户信息");
        //查询数据库看是否存在数据
        LambdaQueryWrapper<UserInformation> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(UserInformation::getUserId,user_id);
        UserInformation userInformationObject = getOne(queryWrapper);
        if (null==userInformationObject){
            return null;
        }
        //查到了数据库中的数据,如果token为空，或者token跟传入的id一致，直接返回用户数据

        User user = copyBean(new User(), userInformationObject);
        if (StringUtils.isBlank(token)){
            user.setIs_follow(false);
            log.info("没有获取到token");
            return user;
        }
        //如果有token,说明用户登录了，需要判断关注状态
        Claims claimsBody = AppJwtUtil.getClaimsBody(token);
        Integer id = (Integer) claimsBody.get("id");
        LambdaQueryWrapper<UserRelationship> relationshipLambdaQueryWrapper=new LambdaQueryWrapper<>();
        relationshipLambdaQueryWrapper.eq(UserRelationship::getUserId,id);
        relationshipLambdaQueryWrapper.eq(UserRelationship::getFollowId,user_id);
        UserRelationship one = relationshipService.getOne(relationshipLambdaQueryWrapper);
        if (null==one){
            //没有关注
            user.setIs_follow(false);
            return user;
        }
        user.setIs_follow(true);
        log.info("用户信息装填完成");
        return user;
    }

    private User copyBean(User user, UserInformation userInformationObject) {
        user.setFollow_count(userInformationObject.getFollowCount());
        user.setId(userInformationObject.getUserId());
        user.setName(userInformationObject.getName());
        user.setFollower_count(user.getFollower_count());
        return user;
    }
}
