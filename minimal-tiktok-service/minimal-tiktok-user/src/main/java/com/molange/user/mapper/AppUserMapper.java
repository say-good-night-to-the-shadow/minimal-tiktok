package com.molange.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.molange.model.user.pojos.AppUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/13 21:16
 */
@Mapper
public interface AppUserMapper extends BaseMapper<AppUser> {
}
