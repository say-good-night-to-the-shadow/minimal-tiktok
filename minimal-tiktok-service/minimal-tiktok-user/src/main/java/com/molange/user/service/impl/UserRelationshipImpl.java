package com.molange.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.molange.model.user.pojos.UserRelationship;
import com.molange.user.mapper.UserRelationshipMapper;
import com.molange.user.service.UserRelationshipService;
import org.springframework.stereotype.Service;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/16 20:57
 *用户粉丝的关系服务实现
 */
@Service
public class UserRelationshipImpl extends ServiceImpl<UserRelationshipMapper, UserRelationship>
        implements UserRelationshipService {
}
