package com.molange.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.molange.model.user.pojos.UserRelationship;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/16 20:56
 */
public interface UserRelationshipService extends IService<UserRelationship> {
}
