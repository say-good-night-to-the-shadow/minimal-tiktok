package com.molange.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.molange.model.user.pojos.UserInformation;
import com.molange.model.user.pojos.UserRelationship;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/16 20:52
 */
@Mapper
public interface UserRelationshipMapper extends BaseMapper<UserRelationship> {
}
