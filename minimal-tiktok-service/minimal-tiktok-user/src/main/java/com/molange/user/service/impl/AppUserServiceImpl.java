package com.molange.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.molange.model.common.ResponseResult;
import com.molange.model.user.pojos.AppUser;
import com.molange.model.user.pojos.UserInformation;
import com.molange.user.mapper.AppUserMapper;
import com.molange.user.mapper.UserInformationMapper;
import com.molange.user.service.AppUserService;
import com.molange.user.utils.AppJwtUtil;
import com.molange.utils.common.SaltUtils;
import com.molange.utils.common.UserNameCreat;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/13 21:21
 * 用户账户服务实现
 */
@Service
@Slf4j
public class AppUserServiceImpl  extends ServiceImpl<AppUserMapper, AppUser> implements AppUserService {

    @Autowired
    private AppUserMapper appUserMapper;

    @Autowired
    private UserInformationMapper userInformationMapper;

    /**
     * 用户注册方法
     * @param username 用户名
     * @param password 密码
     * @return 返回状态
     */
    @Override
    @Transactional
    public ResponseResult register(String username, String password) {
        log.info("用户"+username+"开始进行注册");
        //生成盐值用于加密密码
        String salt = SaltUtils.getSalt(10);
        String saltPassword = DigestUtils.md5DigestAsHex((password + salt).getBytes());
        //进行用户数据的填充
        AppUser appUser=new AppUser();
        appUser.setUsername(username);
        appUser.setPassword(saltPassword);
        appUser.setSalt(salt);
        appUser.setCreatedTime(new Date());
        //0表示正常，1禁用
        appUser.setStatus(0);
        //用户登录成功，返回用户id
        int insert = appUserMapper.insert(appUser);
        //进行用户信息表的进行初始化
        Integer id = appUser.getId();
        UserInformation information=new UserInformation();
        information.setUserId(id);
        information.setFollowCount(0);
        information.setFollowerCount(0);
        information.setName(UserNameCreat.getRandomJianHan(5));
        userInformationMapper.insert(information);
        //用户注册成功，开始进行token的生成
        Map<String,Object> map  = new HashMap<>();
        Long appUserId = new Long((long)insert);
        String token = AppJwtUtil.getToken(appUserId);
        return ResponseResult.okResult(0,appUser.getId(),token,"账号注册成功");
    }

    /**
     * 用户登录方法
     * @param username 用户名
     * @param password 密码
     * @return 返回用户登录状态
     */
    @Override
    public ResponseResult login(String username, String password) {
        log.info("用户"+username+"开始进行登录");
        //查询数据库获取用户数据
        LambdaQueryWrapper<AppUser> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(AppUser::getUsername,username);
        //拿到数据库中对应的用户信息
        AppUser appUser =getOne(queryWrapper);
        if(null==appUser){
            return ResponseResult.errorResult(100,"当前账号未注册");
        }
        //获取对应的盐值
        String appSalt = appUser.getSalt();
        //用盐值对密码进行加密
        String saltPassword = DigestUtils.md5DigestAsHex((password + appSalt).getBytes());
        //与数据库中的数据进行对比
        if (!saltPassword.equals(appUser.getPassword())){
            return ResponseResult.errorResult(100,"用户名或密码错误");
        }
        log.info("用户"+username+"账号密码校验成功");
        //校验账号的状态
        if (!appUser.getStatus().equals(0)){
            return ResponseResult.errorResult(100,"当前账号已被禁用");
        }
        //登录成功，开始生成用户对应的token
        long l = appUser.getId().longValue();
        String token = AppJwtUtil.getToken((Long)l);
        //登录完成
        log.info("登录功能完成，返回token");
        return ResponseResult.okResult(0,appUser.getId(),token,"登录成功");

    }
}
