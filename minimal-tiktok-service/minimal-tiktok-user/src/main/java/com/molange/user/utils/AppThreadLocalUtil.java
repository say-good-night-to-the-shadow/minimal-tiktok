package com.molange.user.utils;


import com.molange.model.user.pojos.AppUser;

public class AppThreadLocalUtil {

    private final static ThreadLocal<AppUser> WM_USER_THREAD_LOCAL = new ThreadLocal<>();

    /**
     * 添加用户
     * @param AppUser
     */
    public static void  setUser(AppUser AppUser){
        WM_USER_THREAD_LOCAL.set(AppUser);
    }

    /**
     * 获取用户
     */
    public static AppUser getUser(){
        return WM_USER_THREAD_LOCAL.get();
    }

    /**
     * 清理用户
     */
    public static void clear(){
        WM_USER_THREAD_LOCAL.remove();
    }
}