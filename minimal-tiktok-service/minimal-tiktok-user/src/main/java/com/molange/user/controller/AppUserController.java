package com.molange.user.controller;

import com.molange.model.common.ResponseResult;
import com.molange.model.user.vo.User;
import com.molange.user.service.AppUserService;
import com.molange.user.service.UserInformationService;
import com.molange.utils.common.CheckEmailUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/13 11:31
 */
@RestController
@RequestMapping("/douyin/user")
public class AppUserController {

    @Autowired
    private AppUserService appUserService;

    @Autowired
    private UserInformationService userInformationService;

    /**
     * 用户登录方法
     * @param username 用户名
     * @return 返回登录状态与token以及用户信息
     */
    @PostMapping("/login")
    public ResponseResult login(String username, String password) {
        //参数校验
        if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
            return ResponseResult.errorResult(100, "用户名或密码不可为空");
        }
        //对用户名（邮箱）进行校验
        if (!CheckEmailUtil.checkEmail(username)) {
            return ResponseResult.errorResult(100, "邮箱格式不正确，请重新输入");
        }
        //判断长度是否符合要求
        if (username.length()>32){
            return ResponseResult.errorResult(100, "邮箱长度不正确，请重新输入");
        }
        //判断密码是否符合要求
        if (password.length() < 5 || password.length() > 32) {
            return ResponseResult.errorResult(100, "密码长度不正确，请重新输入");
        }
        return appUserService.login(username,password);
    }

    /**
     * 用户注册方法
     * @param username 用户名
     * @param password 密码
     * @return 注册状态
     */
    @PostMapping("/register")
    private ResponseResult register(String username, String password) {
        //参数校验
        if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
            return ResponseResult.errorResult(100, "用户名或密码不可为空");
        }
        //对用户名（邮箱）进行校验
        if (!CheckEmailUtil.checkEmail(username)) {
            return ResponseResult.errorResult(100, "邮箱格式不正确，请重新输入");
        }
        //判断长度是否符合要求
        if (username.length()>32){
            return ResponseResult.errorResult(100, "邮箱长度不正确，请重新输入");
        }
        //判断密码是否符合要求
        if (password.length() < 5 || password.length() > 32) {
            return ResponseResult.errorResult(100, "密码长度不正确，请重新输入");
        }
        return appUserService.register(username,password);
    }

    /**
     * 登录或注册完成后返回用户所有的数据
     * @return 用户全部信息
     */
    @GetMapping
    private ResponseResult userMessage(String user_id,String token){
        //参数的校验,token先不验，用户没token也可以看其他人的粉丝
        if (StringUtils.isBlank(user_id)||Integer.parseInt(user_id)<=0){
            return ResponseResult.errorResult(100,"请求参数不合法");
        }
        //线程在的数据已经进行过了验签，需要对比是否是当前用户
      /*  AppUser user = AppThreadLocalUtil.getUser();
        String tokenUserId= String.valueOf(user.getId());
        if (!tokenUserId.equals(user_id)){
            //token中的用户id数据与前端的传入用户id数据不一致,拦截请求
            return ResponseResult.errorResult(100,"非法的用户请求");
        }*/
        //前置参数校验完成，执行业务逻辑
        return userInformationService.getUserInformation(user_id,token);
    }

    @PostMapping("/loadMessage")
    private User userMessageByFeign(String user_id, String token){
        return userInformationService.getUserInformationByFeign(user_id,token);
    }
}
