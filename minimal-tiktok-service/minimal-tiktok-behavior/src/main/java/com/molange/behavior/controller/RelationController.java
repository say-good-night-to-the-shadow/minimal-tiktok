package com.molange.behavior.controller;

import com.molange.behavior.service.RelationService;
import com.molange.model.common.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/2/26 9:48
 */
@RestController
@Slf4j
@RequestMapping("/douyin/relation")
public class RelationController {

    @Autowired
    private RelationService relationService;

    @PostMapping("/action")
    public ResponseResult savaRelation(@RequestParam("token") String token,
                                       @RequestParam("to_user_id") String userId,
                                       @RequestParam("action_type") String actionType){
        if (StringUtils.isBlank(token)||StringUtils.isBlank(userId)||StringUtils.isBlank(actionType)){
            return new ResponseResult(100,"数据参数异常！");
        }
        log.info("开始进行用户关注");
        //数据正常，开始保存用户关注信息
        return relationService.savaUserRelation(token,userId,actionType);

    }
}
