package com.molange.behavior.config;

import com.molange.behavior.interceptor.BehaviorTokenInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/17 14:14
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new BehaviorTokenInterceptor()).addPathPatterns("/**");
    }
}
