package com.molange.behavior.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.molange.model.behavior.pojos.Comment;
import com.molange.model.common.ResponseResult;


/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/2/21 23:03
 */
public interface CommentService extends IService<Comment> {
    ResponseResult saveComment(String token, String publishId, String type, String commentText, String commnetId);

    ResponseResult getCommentList(String token, String publishId);
}
