package com.molange.behavior.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.molange.model.behavior.pojos.LikeBehavior;
import com.molange.model.common.FeedResponseResult;
import com.molange.model.common.ResponseResult;


/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/30 21:38
 */
public interface FavouriteService extends IService<LikeBehavior> {
    ResponseResult likeBehavior(String token, String videoId, String actionType);

    FeedResponseResult getLikeList(String userId, String token);

    Boolean getIslikeBehavior(Integer userId, Integer publishId);
}
