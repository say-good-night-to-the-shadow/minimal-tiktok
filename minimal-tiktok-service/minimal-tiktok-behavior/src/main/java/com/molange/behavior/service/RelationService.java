package com.molange.behavior.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.molange.model.behavior.pojos.LikeBehavior;
import com.molange.model.behavior.pojos.UserRealtion;
import com.molange.model.common.ResponseResult;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/2/26 10:00
 */
public interface RelationService extends IService<UserRealtion> {
    ResponseResult savaUserRelation(String token, String userId, String actionType);

    Boolean isRelationTwo(Integer tokenUserId, String userId);

    Boolean isUserRelationOther(Integer tokenUserId,String userid);

    void setTwoUserRelationTwo(Integer tokenUserId, String userId);

    void deleteTwoUserRelationTwo(Integer tokenUserId, String userId);

}
