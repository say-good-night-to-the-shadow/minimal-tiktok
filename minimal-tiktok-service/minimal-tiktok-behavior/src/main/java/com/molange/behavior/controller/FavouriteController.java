package com.molange.behavior.controller;

import com.molange.behavior.service.FavouriteService;
import com.molange.model.common.FeedResponseResult;
import com.molange.model.common.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/30 21:28
 */
@RestController
@Slf4j
@RequestMapping("/douyin/favorite")
public class FavouriteController {

    @Autowired
    private FavouriteService favouriteService;

    /**
     * 用户点赞行为
     * @param token
     * @param videoId
     * @param actionType
     * @return
     */
    @PostMapping("/action")
    public ResponseResult likeBehavior(@RequestParam("token") String token,
                                       @RequestParam("video_id")String videoId,
                                       @RequestParam("action_type")String actionType
                                       ){
        if (StringUtils.isBlank(token)){
            return  new ResponseResult(100,"请先登录哦");
        }
        if (StringUtils.isBlank(videoId)){
            return  new ResponseResult(100,"非法用户请求");
        }
        if (StringUtils.isBlank(actionType)){
            return  new ResponseResult(100,"非法用户请求");
        }
        return favouriteService.likeBehavior(token,videoId,actionType);
    }

    /**
     * 获取用户喜欢列表
     * @param userId
     * @param token
     * @return
     */
    @GetMapping("/list/")
    public FeedResponseResult likeList(@RequestParam("user_id") String userId,
                                       @RequestParam("token")String token){
        log.info("喜欢列表获取到请求");
        if (StringUtils.isBlank(userId)){
            return new FeedResponseResult(100,"非法请求参数",null,null);
        }
        return favouriteService.getLikeList(userId,token);
    }

    @GetMapping("/behaviorlike")
    public Boolean getIslikeBehavior(@RequestParam("userid") Integer userId,@RequestParam("publishid") Integer publishId){
        if (null==userId||publishId==null){
            return false;
        }
        return favouriteService.getIslikeBehavior(userId,publishId);
    }
}
