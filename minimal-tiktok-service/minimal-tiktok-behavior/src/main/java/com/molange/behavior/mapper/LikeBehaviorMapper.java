package com.molange.behavior.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.molange.model.behavior.pojos.LikeBehavior;
import com.molange.model.publish.pojos.PublishInformation;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/31 15:09
 */
@Mapper
public interface LikeBehaviorMapper extends BaseMapper<LikeBehavior> {
}
