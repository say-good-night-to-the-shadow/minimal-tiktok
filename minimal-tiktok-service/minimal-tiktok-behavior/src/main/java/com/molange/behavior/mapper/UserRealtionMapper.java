package com.molange.behavior.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.molange.model.behavior.pojos.LikeBehavior;
import com.molange.model.behavior.pojos.UserRealtion;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/2/26 10:01
 */
@Mapper
public interface UserRealtionMapper extends BaseMapper<UserRealtion> {
}
