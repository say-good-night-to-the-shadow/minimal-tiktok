package com.molange.behavior.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.molange.model.behavior.pojos.Comment;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/2/21 23:00
 */
@Mapper
public interface CommentMapper extends BaseMapper<Comment> {
}
