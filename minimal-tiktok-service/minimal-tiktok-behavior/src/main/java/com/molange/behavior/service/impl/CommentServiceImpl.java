package com.molange.behavior.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.molange.behavior.mapper.CommentMapper;
import com.molange.behavior.service.CommentService;
import com.molange.feign.client.UserClient;
import com.molange.model.behavior.pojos.Comment;
import com.molange.model.behavior.vo.CommentVo;
import com.molange.model.common.ResponseResult;
import com.molange.model.user.vo.User;
import com.molange.utils.common.AppJwtUtil;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/2/21 23:03
 */
@Service
@Slf4j
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment>
        implements CommentService {

    @Autowired
    private UserClient userClient;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 保存用户的评论
     * @param token       鉴权
     * @param publishId   视频的ID
     * @param type        类型
     * @param commentText 评论的内容
     * @param commnetId   评论的id
     * @return
     */
    @Override
    public ResponseResult saveComment(String token, String publishId, String type, String commentText, String commnetId) {
        log.info("开始添加品论");
        Claims claimsBody = AppJwtUtil.getClaimsBody(token);
        //在这里获取用户登录的token,并获取token的数据
        Integer userId = (Integer) claimsBody.get("id");
        //判断类型，如果是添加评论
        if ("1".equals(type)) {
            //进行添加评论，判断是否存在品论
            if (StringUtils.isBlank(commentText) || commentText.length() > 50) {
                return new ResponseResult(100, "评论不合法！");
            }
            //开始走评论流程
            Comment comment = new Comment();
            if (null == userId) {
                return new ResponseResult(100, "服务器异常，请等待！");
            }
            comment.setCreatTime(new Date());
            comment.setCommentText(commentText);
            comment.setPublishId(Integer.parseInt(publishId));
            comment.setStatus(0);
            comment.setUserId(userId);
            save(comment);
            rabbitTemplate.convertAndSend("tiktok.topic", "behavior.comment", Integer.parseInt(publishId));
            //需要返回内容
            return new ResponseResult(0, "评论发表成功");
        }
        //如果是删除评论
        if ("2".equals(type)) {
            //评论内容的删除
            if (StringUtils.isBlank(commnetId)) {
                return new ResponseResult(100, "请求参数错误！");
            }
            //开始删除内容
            LambdaQueryWrapper<Comment> queryWrapper = new LambdaQueryWrapper();
            queryWrapper.eq(Comment::getPublishId, publishId);
            queryWrapper.eq(Comment::getUserId, userId);
            boolean remove = remove(queryWrapper);
            if (remove) {
                rabbitTemplate.convertAndSend("tiktok.topic", "behavior.removeComment", Integer.parseInt(publishId));

                return new ResponseResult(0, "评论删除成功");
            }
            return new ResponseResult(100, "删除失败");
        }
        return new ResponseResult(100, "出现未知错误");
    }

    /**
     * 获取评论列表
     *
     * @param token     鉴权token
     * @param publishId 视频id
     * @return 集合
     */
    @Override
    public ResponseResult getCommentList(String token, String publishId) {
        log.info("开始获取视频评论列表");
        //获取评论列表
        LambdaQueryWrapper<Comment> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.eq(Comment::getPublishId, publishId);
        List<Comment> commentList = list(queryWrapper);
        //开始遍历评论列表，填充数据
        List<CommentVo> commentVoList = new ArrayList<>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd");
        for (Comment comment : commentList) {
            CommentVo commentVo = new CommentVo();
            commentVo.setId(comment.getId());
            commentVo.setContent(comment.getCommentText());
            //获取对应的用户信息
            User user = userClient.userMessageByFeign(String.valueOf(comment.getUserId()), token);
            //todo 是否关注用户（待编写）
            commentVo.setUser(user);
            String format = simpleDateFormat.format(comment.getCreatTime());
            commentVo.setCreate_date(format);
            commentVoList.add(commentVo);
        }
        return new ResponseResult(0, commentVoList);
    }
}
