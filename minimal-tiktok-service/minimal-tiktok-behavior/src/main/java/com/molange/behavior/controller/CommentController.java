package com.molange.behavior.controller;

import com.molange.behavior.service.CommentService;
import com.molange.model.common.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/2/21 22:58
 */
@RestController
@Slf4j
@RequestMapping("/douyin/comment")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @PostMapping("/action")
    public ResponseResult saveComment(@RequestParam("token")String token,
                                      @RequestParam("video_id")String publishId,
                                      @RequestParam("action_type") String type,
                                      @RequestParam(value = "comment_text",required = false) String commentText,
                                      @RequestParam(value = "comment_id",required = false) String commnetId){
        log.info("收到前端请求！！！！！");
        if (StringUtils.isBlank(token)||StringUtils.isBlank(publishId)||
        StringUtils.isBlank(type)){
            return new ResponseResult(100,"请求异常");
        }
    return commentService.saveComment(token,publishId,type,commentText,commnetId);
    }

    /**
     * 获取所有的评论列表
     * @param token 鉴权
     * @param publishId 视频id
     * @return
     */
    @GetMapping("/list")
    public ResponseResult getCommentList(@RequestParam("token") String token,
                                         @RequestParam("video_id") String publishId){
        if (StringUtils.isBlank(token)||StringUtils.isBlank(publishId)){
            return new ResponseResult(100,"请求参数异常");
        }
        return commentService.getCommentList(token,publishId);

    }

}
