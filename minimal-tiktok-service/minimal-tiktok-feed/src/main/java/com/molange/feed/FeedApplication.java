package com.molange.feed;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/28 16:40
 */
 
@SpringBootApplication
@EnableFeignClients(basePackages = "com.molange.feign.client")
public class FeedApplication {
    public static void main(String[] args) {
        SpringApplication.run(FeedApplication.class,args);
    }
}
