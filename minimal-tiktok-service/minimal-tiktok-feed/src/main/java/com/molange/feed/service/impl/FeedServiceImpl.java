package com.molange.feed.service.impl;

import com.alibaba.fastjson.JSON;
import com.molange.feed.service.FeedService;
import com.molange.feign.client.BehaviorClient;
import com.molange.feign.client.PublishClient;
import com.molange.feign.client.UserClient;
import com.molange.model.common.FeedResponseResult;
import com.molange.model.common.ResponseResult;
import com.molange.model.feed.Video;
import com.molange.model.publish.pojos.Publish;
import com.molange.model.user.pojos.AppUser;
import com.molange.model.user.vo.Author;
import com.molange.model.user.vo.User;
import com.molange.utils.common.AppJwtUtil;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/28 18:23
 */
@Service
@Slf4j
public class FeedServiceImpl implements FeedService {

    @Autowired
    private UserClient userClient;
    //做些测试
    @Autowired
    private PublishClient publishClient;
    @Autowired
    private BehaviorClient behaviorClient;


    /**
     * 获取视频流集合接口
     *
     * @param dateTime 时间
     * @param token    token值
     * @return
     */
    @Override
    public FeedResponseResult getFeedList(String dateTime, String token) {
        log.info("开始获取视频流");
        //如果没有token
        if (StringUtils.isBlank(token)) {
            //说明这时候用户是没有登录的，我们返回30条数
            Date date = new Date();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String format = simpleDateFormat.format(date);
            log.info("开始调用feign接口获取数据");
            List<Publish> publish = publishClient.getPublish(format, 30);
            log.info("视频数据已获取");
            List<Video> video_list = new ArrayList<>();
            //遍历视频数据
            extracted(token, publish, video_list);
            Date nowDate = new Date();
            String s2 = date.toString();
            log.info("时间" + s2);
            String data = JSON.toJSONString(video_list);
            return new FeedResponseResult(0, "成功", video_list, 0);
        }
        //说明用户登录了
        //有token,判断token是否有效
        Claims claimsBody = AppJwtUtil.getClaimsBody(token);
        //在这里获取用户登录的token,并获取token的数据
        Integer userId = (Integer) claimsBody.get("id");
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = simpleDateFormat.format(date);
        log.info("开始调用feign接口获取数据");
        List<Publish> publish = publishClient.getPublish(format, 30);
        log.info("视频数据已获取");
        List<Video> video_list = new ArrayList<>();
        //遍历视频数据
        extracted1(token, publish, video_list, userId);
        log.info("获取完成！");
        System.out.println(video_list);
        Date nowDate = new Date();
        long time = nowDate.getTime();
        System.out.println(video_list);
        String s = JSON.toJSONString(video_list);
        return new FeedResponseResult(0, "成功", video_list, 0);
    }

    private void extracted1(String token, List<Publish> publish, List<Video> video_list, Integer userId) {
        for (Publish publish1 : publish) {
            Author author = new Author();
            Video video = new Video();
            video.setId(publish1.getId());
            video.setTitle(publish1.getTitle());
            video.setPlay_url(publish1.getUrl());
            //将user的id与publish中的id一起进行查验，判断是否登录
            Boolean islikeBehavior = behaviorClient.getIslikeBehavior(userId, publish1.getId());
            video.setIs_favorite(islikeBehavior);//设置是否点赞
            log.info("这是视频链接地址" + publish1.getUrl());
            video.setCover_url(publish1.getPictureUrl());
            video.setComment_count(134283);//视频评论
            video.setFavorite_count(103420);//视频点赞
            User userData = userClient.userMessageByFeign(publish1.getUserId().toString(), token);
            author.setIs_follow(userData.is_follow());
            author.setFollow_count(userData.getFollow_count());
            author.setId(userData.getId());
            author.setName(userData.getName());
            author.setFollower_count(userData.getFollower_count());
            video.setAuthor(author);
            video_list.add(video);
        }
    }

    /**
     * 没有token的登录逻辑
     *
     * @param token
     * @param publish
     * @param video_list
     */
    private void extracted(String token, List<Publish> publish, List<Video> video_list) {
        for (Publish publish1 : publish) {
            Author author = new Author();
            Video video = new Video();
            video.setId(publish1.getId());
            video.setTitle(publish1.getTitle());
            video.setPlay_url(publish1.getUrl());
            video.setIs_favorite(false);
            video.setCover_url(publish1.getPictureUrl());
            video.setComment_count(134283);//视频评论
            video.setFavorite_count(103420);//视频点赞
            User userData = userClient.userMessageByFeign(publish1.getUserId().toString(), token);
            author.setIs_follow(userData.is_follow());
            author.setFollow_count(userData.getFollow_count());
            author.setId(userData.getId());
            author.setName(userData.getName());
            author.setFollower_count(userData.getFollower_count());
            video.setAuthor(author);
            video_list.add(video);
        }
    }
}
