package com.molange.feed.service;

import com.molange.model.common.FeedResponseResult;
import com.molange.model.common.ResponseResult;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 获取视频流集合的接口，用于首页的内容的展示
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/28 18:22
 */
public interface FeedService {
    FeedResponseResult getFeedList(String dateTime, String token);
}
