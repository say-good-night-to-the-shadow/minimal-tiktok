package com.molange.feed.controller;

import com.molange.model.common.FeedResponseResult;
import com.molange.model.common.ResponseResult;
import com.molange.feed.service.FeedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/28 16:45
 */
@RestController
@RequestMapping("/douyin/feed")
public class FeedController {

    @Autowired
    private FeedService feedService;

    /**
     * 视频流请求接口
     * @param dateTime 最近时间
     * @param token token
     * @return
     */
    @GetMapping()
    private FeedResponseResult feed(@RequestParam("latest_time")String dateTime,
                                    @RequestParam("token")String token){
        return feedService.getFeedList(dateTime,token);
    }
}
