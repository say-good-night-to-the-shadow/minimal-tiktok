package com.molange.message.utils;

import com.molange.model.user.pojos.AppUser;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/18 12:06
 */
public class MessageThreadLocalUtil {

        private final static ThreadLocal<AppUser> WM_USER_THREAD_LOCAL = new ThreadLocal<>();

        /**
         * 添加用户
         * @param AppUser
         */
        public static void  setUser(AppUser AppUser){
            WM_USER_THREAD_LOCAL.set(AppUser);
        }

        /**
         * 获取用户
         */
        public static AppUser getUser(){
            return WM_USER_THREAD_LOCAL.get();
        }

        /**
         * 清理用户
         */
        public static void clear(){
            WM_USER_THREAD_LOCAL.remove();
        }
}
