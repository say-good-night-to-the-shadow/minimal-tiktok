package com.molange.feign.client;

import com.molange.model.user.vo.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "userservice")
public interface UserClient {

    /**
     * 获取用户信息的接口
     * @param user_id
     * @param token
     * @return
     */
    @PostMapping("/douyin/user/loadMessage")
    User userMessageByFeign(@RequestParam("user_id") String user_id,
                            @RequestParam("token") String token);

}
