package com.molange.feign.client;


import com.molange.model.publish.pojos.Publish;
import com.molange.model.publish.pojos.PublishInformation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/29 19:37
 */
@FeignClient(value = "publishservice")
public interface PublishClient {
    /**
     * 获取视频链接
     * @param date
     * @param num
     * @return
     */
    @PostMapping("/douyin/publish/load")
    List<Publish> getPublish(@RequestParam("data") String date, @RequestParam("num") int num);

    @GetMapping("/douyin/publish/gtePublishDataInformation")
    Publish gtePublishData(@RequestParam("publishId") Integer publishId);

    @GetMapping("/douyin/publish/getPublishInformation")
    PublishInformation getPublishInformation(@RequestParam("publishId") Integer publishId);
}
