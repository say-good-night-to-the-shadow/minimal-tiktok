package com.molange.feign.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/2/23 18:58
 */
 
@FeignClient(value = "behaviorservice")
public interface BehaviorClient {
    @GetMapping("/douyin/favorite/behaviorlike")
    public Boolean getIslikeBehavior(
             @RequestParam("userid") Integer userId,
             @RequestParam("publishid") Integer publishId);
}
