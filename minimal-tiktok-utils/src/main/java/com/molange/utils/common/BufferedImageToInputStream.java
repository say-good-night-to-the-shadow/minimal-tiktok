package com.molange.utils.common;

import lombok.extern.slf4j.Slf4j;
 
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/30 10:06
 */
@Slf4j
public class BufferedImageToInputStream {
    /**
     * 将BufferedImage转换为InputStream
     * @param image
     * @return
     */
    public static InputStream bufferedImageToInputStream(BufferedImage image){
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            ImageIO.write(image, "png", os);
            InputStream input = new ByteArrayInputStream(os.toByteArray());
            return input;
        } catch (IOException e) {
            log.info("bufferedImageToInputStream"+"出现问题"+e);
        }
        return null;
    }
}
