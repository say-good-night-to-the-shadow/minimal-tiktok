package com.molange.utils.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author wangshiqi
 * @version 1.0
 * @date 2023/1/14 11:57
 * 用于邮箱格式的校验
 */
public class CheckEmailUtil {

    public static boolean checkEmail(String email) {
        boolean flag = false;
        try {
            String check = "^([a-z0-9A-Z]+[-|_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
            Pattern regex = Pattern.compile(check);
            Matcher matcher = regex.matcher(email);
            flag = matcher.matches();
        } catch (Exception e) {
            flag = false;
        }
        return flag;
    }
}
