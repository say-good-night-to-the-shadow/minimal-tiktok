package com.molange.utils.common;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.Java2DFrameConverter;

public class VideoUtil {
    /**
     * 获取视频中的图片
     * @param inputStream 视频输入流
     * @return
     * @throws Exception
     */
    public static BufferedImage grabberVideoFramer(InputStream inputStream) throws Exception {
        // 最后获取到的视频的图片缓存
        BufferedImage bufferedImage = null;
        // Frame对象
        Frame frame = null;
        // 标识
        int flag = 0;
        FFmpegFrameGrabber fFmpegFrameGrabber = null;
        try {
            //获取视频文件
            fFmpegFrameGrabber = new FFmpegFrameGrabber(inputStream);
            fFmpegFrameGrabber.start();

            // 获取视频总帧数
            int ftp = fFmpegFrameGrabber.getLengthInFrames();
            //对视屏 帧数处理
            while (flag <= ftp) {
                frame = fFmpegFrameGrabber.grabImage();
                //对视频的第50帧进行处理
                if (frame != null && flag == ftp/50) {
                    // 图片缓存对象
                    bufferedImage = FrameToBufferedImage(frame);
                    break;
                }
                flag++;
            }
        }finally {
            if(fFmpegFrameGrabber != null) {
                fFmpegFrameGrabber.stop();
                fFmpegFrameGrabber.close();
            }
        }
        return bufferedImage;
    }

    /**
     * @param frame 读取视频中每一帧图片
     * @return java.awt.image.BufferedImage
     * @description TODO 将获取的帧，存储为图片
     * @author WangTianLiang
     * @date 2021/6/25
     */
    private static BufferedImage FrameToBufferedImage(Frame frame) {
        // 创建BufferedImage对象 创建一个帧-->图片的转换器
        Java2DFrameConverter converter = new Java2DFrameConverter();
        //转换
        BufferedImage bufferedImage = converter.getBufferedImage(frame);
        return bufferedImage;
    }


    public static void main(String[] args) {
        long time1 = System.currentTimeMillis();
        //视频路径
        String videoFileName = "D:\\Video\\测试一.mp4";
        InputStream intInputStream = null;
        //输出的图片路径
        File outPut = new File("D:\\Video\\测试一.jpg");
        try {
            intInputStream = new FileInputStream(new File(videoFileName));
            BufferedImage bufferedImage = grabberVideoFramer(intInputStream);
            long time2 = System.currentTimeMillis();
            ImageIO.write(bufferedImage, "jpg", outPut);
            System.out.println(time2-time1);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(intInputStream !=null) {
                try {
                    intInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}