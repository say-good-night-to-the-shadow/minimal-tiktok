# MinimalTiktok

#### 介绍
第五届字节青训营后端专场-抖音极简版后台
功能分为三大模块
1.  基础接口（视频流接口，登录，注册，用户信息，发布列表）
2.  互动接口（点赞，喜欢，评论，评论列表）
3.  社交接口（关注，关注列表，粉丝列表，好友列表，发送消息，聊天）

开发文档：https://www.apifox.cn/apidoc/shared-09d88f32-0b6c-4157-9d07-a36d32d7a75c/api-50707522
数据库结构如下：https://www.molange.com/wp-content/uploads/2023/04/sql.zip


#### 软件架构
软件架构说明：mysql、spring、springMVC、springBoot、mybatis、mybatisPlus、springCloud、minio、nacos、Gateway、RabbitMQ、Redis
Caffeine、sentinel

模块介绍：
feign-api 对feign进行了优化抽取出的模块，负责服务之间的远程调用
minimal-tiktok-app-gateway 网关模块：负责用户请求路由断言与负载均衡，同时对token经行验签
minimal-tiktok-model 公共实体类，存放数据库对应的实体类，pojo,vo,dto
minimal-tiktok-service 服务模块

第三方：阿里云视频内容审核Api与文字审核

业务逻辑：
通过nacos实现服务的注册与发现，将微服务模块与网关注册到nacos,通过gateway实现请求的路由与断言并完成token的统一验签，同时实现服务负载均衡
不同服务之间通过feign进行调用，同时通过抽取与添加httpclient对feign进行优化增强，同时通过添加快速失败策略，实现服务的高可用


#### 安装教程

1.  下载安装抖音极简版app

#### 使用说明

1.  下载安装抖音极简版app

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用：minio存储用户视频内容，并支持在线预览。
2.  通过工具类ffmpeg、javacv实现用户视频封面截取（默认为第50帧）
3.  通过jwt与加密算法实现token内容加密，确保数据安全
4.  使用redis与nacos配置中心实现数据读写降级功能，确保核心服务可用性，热点数据缓存
5.  使用xxl-job实现降级数据的定时写入数据库
6.  使用rabbitMQ实现流量效削峰

 待使用------------------------------------------------------------------

7.  使用seata实现分布式事务
8.  redis与Caffeine搭配设置二级缓存
更多功能待优化... ...